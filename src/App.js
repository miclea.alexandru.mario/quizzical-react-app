import React from "react"
import Question from "./Question"

export default function App() {
    const [start, setStart] = React.useState(false)
    const [questionsData, setQuestionsData] = React.useState([])   
    const [showResults, setShowResults] = React.useState(false)
    const [score, setScore] = React.useState(0)
    
    function decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
    
    
    function shuffle(array) {
        let currentIndex = array.length,  randomIndex
        while (currentIndex != 0) {

            randomIndex = Math.floor(Math.random() * currentIndex)
            currentIndex--

            [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]]
        }

        return array
    }
    
    
    React.useEffect(() => {
        if (!start) {
            fetch("https://opentdb.com/api.php?amount=5&category=9&difficulty=easy&type=multiple")
                .then(res => {
                    if (!res.ok) {
                        throw new Error("Network response was not ok");
                    }
                    return res.json();
                })
                .then(data => {
                    const formattedQuestions = data.results.map((question, index) => {
                        const allAnswers = [...question.incorrect_answers, question.correct_answer];
                        return {
                            id: index,
                            question: decodeHtml(question.question),
                            answers: shuffle(allAnswers),
                            correctAnswer: question.correct_answer,
                            selectedAnswer: ""
                        };
                    });
                    setQuestionsData(formattedQuestions);
                })
                .catch(error => {
                    console.error("Error fetching questions:", error);
                });
        }
    }, [start]);
    
    function selectAnswer(answer, id) {
        if(!showResults)
            setQuestionsData(oldQuestionsData => oldQuestionsData.map(question => {
                return question.id === id ? 
                    {...question, "selectedAnswer": answer} :
                    question
            }))
    }
    
    function checkAnswers() {
        if(showResults) {
            setShowResults(false)
            setStart(false)
        } else {
            let score = 0
            for(let i = 0; i < 5; i++) {
                if(questionsData[i].correctAnswer === questionsData[i].selectedAnswer)
                    score++
            }
            setScore(score)
            setShowResults(true)            
        }
    }
    
    return (
        <main className={!start ? "start" : ""}>
            {!start ? <div className="startScreen">
                <h1>Quizzical</h1>
                <p>Some description if needed</p>
                <button onClick={() => setStart(true)}>Start quiz</button>
            </div> : 
                <div className="questionsContainer">
                    {questionsData.map((question) =>
                            <Question
                                key={question.id}
                                id={question.id}
                                question={question.question}
                                correctAnswer={question.correctAnswer}
                                answers={question.answers}
                                selectedAnswer={question.selectedAnswer}
                                selectAnswer={selectAnswer}
                                showResults={showResults}
                            />
                        
                    )}
                    <div className="answerBtnContainer">
                        {showResults && <p>You scored {score}/5 correct answers</p>}
                        <button onClick={checkAnswers}>{!showResults ? "Check answers" : "Play again"}</button>
                    </div>
                </div>
            }
            <div className="topCircle"></div>
            <div className="bottomCircle"></div>
        </main>
    )
}