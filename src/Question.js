import React from "react"

export default function Question(props) {
    function decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
    
    function shuffle(array) {
        let currentIndex = array.length,  randomIndex
        while (currentIndex != 0) {

            randomIndex = Math.floor(Math.random() * currentIndex)
            currentIndex--

            [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]]
        }

        return array
    }
    
    
    const showAnswers = props.answers.map((answer) => {
        return (
        <div onClick={() => props.selectAnswer(answer, props.id)} className={`answer ${props.showResults && answer === props.correctAnswer ? "correct" : props.showResults && props.selectedAnswer === answer && props.selectedAnswer != props.correctAnswer ? "incorrect" : props.selectedAnswer === answer && !props.showResults ? "selected" : props.showResults ? "disabled" : ""}`} key={answer}>{decodeHtml(answer)}</div> )
    })
    
    return (
        <div className="question">
            <h1>{props.question}</h1>
            <div className="answers">
               {showAnswers}
            </div>
        </div>
    )
}